#!/bin/bash

echo "Start Dbus"
/etc/init.d/dbus start

echo "Start Bluetooth"
bluetoothd -d &

echo "Start daemon"
mqtt-ble
