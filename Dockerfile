FROM docker.io/arm32v7/python:3.9-buster

COPY requirements.txt /requirements.txt

RUN apt-get update -y && \
    apt-get install -y dbus bluez --no-install-recommends && \
    apt-get install -y libglib2.0-dev make gcc --no-install-recommends && \
    pip install -r /requirements.txt && \
    apt-get remove --purge -y libglib2.0-dev make gcc && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt

#RUN apt-get update -y
#RUN apt-get install -y dbus bluez libglib2.0-dev make gcc --no-install-recommends
#RUN pip install -r /requirements.txt

#apt-get remove --purge -y libglib2.0-dev make gcc python3.5 python3.5-minimal && \

RUN mkdir -p /app
WORKDIR /app

COPY Dockerfile /Dockerfile
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY get_key.py /app/get_key.py
COPY requirements.txt  setup.py  test_requirements.txt /app/
RUN pip install -r /app/requirements.txt
COPY mqtt_ble /app/mqtt_ble

RUN python setup.py install

ENV MQTT_USERNAME=hass
ENV MQTT_PASSWORD=hass
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV LOG_LEVEL=INFO

CMD bash /entrypoint.sh
