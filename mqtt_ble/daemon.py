"""BLEDaemon module."""
import asyncio
import os

import yaml
from bleak import BleakScanner
from bleak.exc import BleakDBusError, BleakError
from mqtt_hass_base.daemon import MqttClientDaemon

from mqtt_ble import cleargrass, decora, nespresso

MAIN_LOOP_WAIT_TIME = 60


class BLEDaemonError(Exception):
    """Base error."""


class BLEDaemon(MqttClientDaemon):
    """BLEDaemon class."""

    _devices = []

    def __init__(self):
        """Create the main daemon."""
        MqttClientDaemon.__init__(self, "mqtt-ble")

    def read_config(self):
        """Read config file."""
        self.config_file = os.environ["CONFIG_YAML"]
        with open(self.config_file) as fhc:
            self.config = yaml.safe_load(fhc)
        self.sync_frequency = int(
            self.config.get("sync_frequency", MAIN_LOOP_WAIT_TIME)
        )
        self.update_interval = self.config.get("update_interval", MAIN_LOOP_WAIT_TIME)
        self._wait_time = self.update_interval + 1

    def add_devices(self):
        """Create devices based on the configuration."""
        for device_conf in self.config.get("devices"):
            # self.logger.info("Adding device: %s",
            device_model_cleaned = device_conf["model"].capitalize()
            if "update_interval" not in device_conf:
                device_conf["update_interval"] = self.update_interval
            if hasattr(decora, device_model_cleaned):
                device = getattr(decora, device_model_cleaned)(device_conf, self.logger)
            elif hasattr(nespresso, device_model_cleaned):
                device = getattr(nespresso, device_model_cleaned)(
                    device_conf, self.logger
                )
            elif hasattr(cleargrass, device_model_cleaned):
                device = getattr(cleargrass, device_model_cleaned)(
                    device_conf, self.logger
                )
            else:
                self.logger.error("Device %s not supported", device_conf["model"])
                continue
            # TODO check if the device name is not taken
            self._devices.append(device)

    async def try_device_connection(self):
        """Try to connect to the device."""
        self.logger.info("Starting trying to connect to devices")
        # TODO ADD caching
        for device in self._devices:
            self.logger.info("Starting trying to connect %s", device.name)
            if not device.ready:
                try:
                    await device.init_connection()
                except asyncio.exceptions.TimeoutError as exp:
                    self.logger.error(
                        "Can not connect to device %s: %s", device.name, exp
                    )
                    continue
                except BleakError as exp:
                    self.logger.error(
                        "Can not connect to device %s: %s", device.name, exp
                    )
                    continue
                except BleakDBusError as exp:
                    self.logger.error(
                        "Can not connect to device %s: %s", device.name, exp
                    )
                    continue
                device.set_mqtt_client(self.mqtt_client)
                device.register()
                device.subscribe()
        self.connect_th = None
        self.logger.info("Stopping trying to connect to devices")

    def passive_monitor_callback(self, sender, advertisement_data):
        """Received passive data callback."""
        if sender.address.lower() in [
            d.client.mac for d in self._devices if d.is_passive
        ]:
            device = [
                d for d in self._devices if sender.address.lower() == d.client.mac
            ][0]
            if device.ready:
                device.push_data(advertisement_data)
            else:
                self.logger.warning("Device %s not ready", device.name)

    async def passive_monitor(self):
        """Start passive monitoring."""
        scanner = BleakScanner()
        scanner.register_detection_callback(self.passive_monitor_callback)
        self.logger.debug("Starting passive monitor")
        await scanner.start()
        await asyncio.sleep(5)
        await scanner.stop()
        self.logger.debug("Stopping passive monitor")

    async def _init_main_loop(self):
        """Run before the main loop."""
        self.add_devices()
        # loop = asyncio.get_event_loop()
        self.scanner = BleakScanner()
        self.scanner.register_detection_callback(self.passive_monitor_callback)
        await self.scanner.start()

    async def _main_loop(self):
        """Run main loop of the daemon."""
        if self._wait_time > self.update_interval:
            if not all([d.ready for d in self._devices]):
                self._wait_time = 0
                await self.try_device_connection()

        # Manage non passive device
        for device in self._devices:
            if device.ready and not device.is_passive:
                await device.update()

        # Check availability of passive device
        for device in self._devices:
            if device.ready and device.is_passive:
                device.check_availability()

        # if self.must_run:
        await asyncio.sleep(1)
        self._wait_time += 1

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""
        for device in self._devices:
            device.unregister()

    def _on_message(self, client, userdata, msg):
        """Do nothing."""

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        await self.scanner.stop()
        for device in self._devices:
            await device.disconnect()
