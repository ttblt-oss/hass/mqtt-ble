"""Module defining entrypoint."""
import asyncio

from mqtt_ble.daemon import BLEDaemon


def main():
    """Entrypoint function."""
    dev = BLEDaemon()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
