"""Decora ddl06-blz device module."""
import asyncio
import json

from mqtt_hass_base.device import MqttDevice

from mqtt_ble.common import BTDevice

_UUID_CHAR_NAME = "00002A00-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_MODEL = "00002A24-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_FIRMWARE = "00002A26-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_HARDWARE = "00002A27-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_SOFTWARE = "00002A28-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_MANUFACTURER = "00002A29-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_OTHER1 = "0000FF06-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_OTHER2 = "0000FF09-0000-1000-8000-00805F9B34FB"
_UUID_SERVICE_DEVICE_INFO = "0000180A-0000-1000-8000-00805F9B34FB"
_UUID_SERVICE_GENERIC_ACCESS = "00001800-0000-1000-8000-00805F9B34FB"
_UUID_SERVICE_DATA = "22210000-554A-4546-5542-46534450464D"
_UUID_CHAR_DATA = "0000ff01-0000-1000-8000-00805f9b34fb"


class DecoraException(Exception):
    """Decora Exception class."""


class Ddl06blzBT(BTDevice):
    """Decora DDL06-BLZ device class."""

    def __init__(self, mac, logger, key, notification_timeout=5.0, iface=0):
        """Create a Decora DDL06-BLZ device object."""
        BTDevice.__init__(
            self,
            mac,
            logger,
            keep_connected=True,
            notification_timeout=5.0,
            iface=iface,
        )
        self._device_info_uuids["_name"] = "0000ff09-0000-1000-8000-00805f9b34fb"
        self.config = {}
        self.level = 0
        self.power = 0
        if isinstance(key, str):
            self.key = int(key).to_bytes(4, byteorder="big")
        elif isinstance(key, int):
            self.key = key.to_bytes(4, byteorder="big")
        else:
            self.key = key

    async def unlock(self):
        """Unlock device."""
        await self.set_event(83, self.key)

    @BTDevice.ensure_connected
    async def set_event(self, event, data):
        """Set a bluetooth event."""
        packet = bytearray([0x11, event, data[0], data[1], data[2], data[3]])
        # TODO add try
        await self.write_gatt_char(self.handles["event"], packet, response=True)

    @BTDevice.ensure_connected
    async def get_config(self):
        """Read device configuration."""
        # TODO add try
        data = await self.read_gatt_char(self.handles["config1"])
        self.config["bulbtype"] = data[0]
        self.config["maxoutput"] = data[1]
        self.config["minoutput"] = data[2]
        self.config["defaultoutput"] = data[3]
        # TODO add try
        data = await self.read_gatt_char(self.handles["config2"])
        self.config["fadeon"] = data[0]
        self.config["fadeoff"] = data[1]
        self.config["ledtimeout"] = data[2]

    @BTDevice.ensure_connected
    async def fetch_data(self):
        """Fetch current data."""
        # TODO add try
        data = await self.read_gatt_char(self.handles["state"])
        self.power = data[0]
        self.level = data[1]

    def _save_handles(self, characteristics):
        """Read handles."""
        for characteristic in characteristics.values():
            if characteristic.uuid == "0000ff01-0000-1000-8000-00805f9b34fb":
                self.handles["state"] = characteristic
            if characteristic.uuid == "0000ff02-0000-1000-8000-00805f9b34fb":
                self.handles["config1"] = characteristic
            if characteristic.uuid == "0000ff03-0000-1000-8000-00805f9b34fb":
                self.handles["config2"] = characteristic
            if characteristic.uuid == "0000ff04-0000-1000-8000-00805f9b34fb":
                self.handles["location1"] = characteristic
            if characteristic.uuid == "0000ff05-0000-1000-8000-00805f9b34fb":
                self.handles["location2"] = characteristic
            if characteristic.uuid == "0000ff06-0000-1000-8000-00805f9b34fb":
                self.handles["event"] = characteristic
            if characteristic.uuid == "0000ff07-0000-1000-8000-00805f9b34fb":
                self.handles["time"] = characteristic
            if characteristic.uuid == "0000ff08-0000-1000-8000-00805f9b34fb":
                self.handles["data"] = characteristic
            if characteristic.uuid == "0000ff09-0000-1000-8000-00805f9b34fb":
                self.handles["name"] = characteristic

    @BTDevice.ensure_connected
    async def set_state(self):
        """Set bluetooth state."""
        packet = bytearray([self.power, self.level])
        await self.write_gatt_char(self.handles["state"], packet, response=True)

    async def turn_off(self):
        """Turn off the switch."""
        await self.fetch_data()
        self.power = 0
        await self.set_state()

    async def turn_on(self):
        """Turn on the switch."""
        await self.fetch_data()
        self.power = 1
        await self.set_state()

    async def set_brightness(self, level):
        """Set light brightness."""
        await self.fetch_data()
        self.level = level
        await self.set_state()


class Ddl06blz(MqttDevice):
    """DDL06-BLZ MQTT device class."""

    def __init__(self, config, logger, iface=0):
        """Create a new MQTT Decora DDL06-BLZ light object."""
        MqttDevice.__init__(self, config["name"], logger)
        self.config = config
        try:
            key = int(config["key"], base=16)
        except ValueError:
            try:
                key = int(config["key"], base=10)
            except ValueError:
                raise Exception("Bad Bluetooth key")
        self._update_interval = config["update_interval"]
        self._wait_time = 0
        self.client = Ddl06blzBT(config["mac"], logger, key, iface=iface)
        self._ready = False
        self._failed_connection = 0
        self.loop = asyncio.get_event_loop()

    @property
    def ready(self):
        """Return True if connected to the device."""
        return self._ready and self.client.is_connected

    @property
    def is_passive(self):
        """Return True if it's a passive BLE device."""
        return False

    async def init_connection(self):
        """Connect to the device."""
        await self.client.get_handles()
        await self.client.unlock()
        await self._get_device_info()
        self._add_entities()
        await self.client.start_notify(
            self.client.handles["state"], self.state_callback_handler
        )

    async def state_callback_handler(self, sender, data):
        """State notify Bluetooth callback.

        This function is called when the bluetooth device send
        data at the state service.
        """
        self._failed_connection = 0
        self.client.power = data[0]
        self.client.level = data[1]
        state = {
            "brightness": self.client.level,
            "state": "OFF" if self.client.power == 0 else "ON",
        }
        self.light.send_state(state)
        self.light.send_available()

    async def _get_device_info(self):
        """Get device information."""
        await self.client.device_info()
        self.mac = self.config["mac"]
        self.manufacturer = self.client.manufacturer
        self.sw_version = self.client.firmware_version
        self.model = self.client.model
        await self.client.get_config()
        self._ready = True

    def _add_entities(self):
        """Add light entity to Home Assistant through MQTT."""
        entity_settings = {
            "optimistic": False,
            "brightness": True,
            "brightness_scale": self.client.config["maxoutput"],
        }

        cleaned_name = " ".join(
            (
                "mqtt-ble",
                self.config["name"].lower().replace(" ", "_"),
                "light",
            )
        )

        subs = {
            "command_topic": self._callback_command,
        }
        self.light = self.add_entity(
            "light",
            cleaned_name,
            entity_settings,
            subs,
        )

    async def update(self, force=False):
        """Fetch device state."""
        if not force and self._wait_time < self._update_interval:
            self._wait_time += 1
            return

        self.logger.debug("Updating device %s", self.name)
        self._wait_time = 0

        # TODO add try:
        await self.client.fetch_data()
        #        try:
        #            await self.client.fetch_data()
        #        except btle.BTLEDisconnectError as exp:
        #            self.logger.warning("Error updating %s: %s", self.name, exp)
        #            self._failed_connection += 1
        #            return
        self._failed_connection = 0
        state = {
            "brightness": self.client.level,
            "state": "OFF" if self.client.power == 0 else "ON",
        }
        self.light.send_state(state)
        self.light.send_available()

    def _callback_command(
        self, client, userdata, msg
    ):  # pylint: disable=unused-argument
        """Handle MQTT message from subscriptions."""
        self.logger.debug("Command received: %s", msg.payload)
        payload = json.loads(msg.payload)
        task = None
        if payload["state"] == "OFF" and self.client.power == 1:
            task = self.loop.create_task(self.client.turn_off())
        elif payload["state"] == "ON" and self.client.power == 0:
            task = self.loop.create_task(self.client.turn_on())
        if "brightness" in payload:
            task = self.loop.create_task(
                self.client.set_brightness(payload["brightness"])
            )
        self.logger.debug("Command handled by task %s", task)

    async def disconnect(self):
        """Disconnect from device."""
        self.logger.info("BLE disconnecting")
        await self.client.disconnect()
