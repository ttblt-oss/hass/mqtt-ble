"""Common module defining base classes."""
import inspect
import typing
from datetime import datetime
from threading import Lock

from bleak import BleakClient
from bleak.exc import BleakError

_UUID_CHAR_OTHER1 = "0000FF06-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_OTHER2 = "0000FF09-0000-1000-8000-00805F9B34FB"
_UUID_SERVICE_DEVICE_INFO = "0000180A-0000-1000-8000-00805F9B34FB"
_UUID_SERVICE_GENERIC_ACCESS = "00001800-0000-1000-8000-00805F9B34FB"

DEVICE_INFO_UUIDS = {
    "_name": "00002A00-0000-1000-8000-00805F9B34FB",
    "_manufacturer": "00002A29-0000-1000-8000-00805F9B34FB",
    "_model": "00002A24-0000-1000-8000-00805F9B34FB",
    "_firmware": "00002A26-0000-1000-8000-00805F9B34FB",
    "_hardware": "00002A27-0000-1000-8000-00805F9B34FB",
    "_software": "00002A28-0000-1000-8000-00805F9B34FB",
}


class BTDevice(BleakClient):
    """Base class for Bluetooth device."""

    _device_info_last_read: typing.Optional[datetime] = None
    _name = None
    _model = None
    _manufacturer = None
    _firmware_version = None
    _hardware_version = None
    _software_version = None

    def __init__(  # dpylint: disable=super-init-not-called
        self, mac, logger, keep_connected=False, notification_timeout=5.0, iface="hci0"
    ):
        """Initialize a Poller for the given MAC address."""
        BleakClient.__init__(self, mac, device=iface)
        self.logger = logger.getChild("btclient")
        self._mac = mac
        self._keep_connected = keep_connected
        self._notification_timeout = notification_timeout
        self._iface = iface
        self.lock = Lock()
        self.handles = {}
        self._device_info_uuids = DEVICE_INFO_UUIDS

    def _read_char(self, char_uuid, service_uuid=None):
        """Read data from bluetooth."""
        if not service_uuid:
            self.logger.debug(
                "Reading characteristic %s of service %s", char_uuid, service_uuid
            )
            service = self._peripheral.getServiceByUUID(service_uuid)
            char = service.getCharacteristics(forUUID=char_uuid)[0]
        else:
            self.logger.debug("Reading characteristic %s", char_uuid)
            char = self._peripheral.getCharacteristics(uuid=char_uuid)[0]
        data = char.read()
        self.logger.debug("Read: %s", data)
        return data

    def ensure_connected(method):  # pylint: disable=no-self-argument
        """Ensure bluetooth connection."""
        # Decorator function
        async def wrapper(self, *args, **kwargs):
            if not self.is_connected:
                self.logger.warning(
                    "Not connected to %s, we're reconnecting now", self._mac
                )
                await self.connect()
            if inspect.iscoroutinefunction(method):
                ret = await method(
                    self, *args, **kwargs
                )  # pylint: disable=not-callable
            else:
                ret = method(self, *args, **kwargs)  # pylint: disable=not-callable
            if not self._keep_connected:
                await self.disconnect()
            return ret

        return wrapper

    @ensure_connected
    async def get_handles(self):
        """Read handles method."""
        try:
            services = await self.get_services()
        except BleakError:
            raise Exception("Unable to list services")
        self._save_handles(services.characteristics)

    def _save_handles(self, characteristics):
        """Save handles."""
        raise NotImplementedError

    @ensure_connected
    async def device_info(self):
        """Get device information."""
        self.logger.debug("Getting device information of %s", self._mac)
        for attr_name, uuid_char in self._device_info_uuids.items():
            try:
                value = await self.read_gatt_char(uuid_char)
                setattr(self, attr_name, value.decode().strip("\x00").strip())
            except BleakError as exp:
                self.logger.warning("%s - name: %s", exp, attr_name)

    @property
    def mac(self):
        """Return the mac of the device."""
        return self._mac.lower()

    @property
    def name(self):
        """Return the name of the device."""
        return self._name

    @property
    def model(self):
        """Return the model of the device."""
        return self._model

    @property
    def manufacturer(self):
        """Return the manufacturer of the device."""
        return self._manufacturer

    @property
    def firmware_version(self):
        """Return the firmware version of the device."""
        return self._firmware_version

    @property
    def hardware_version(self):
        """Return the hardware version of the device."""
        return self._hardware_version

    @property
    def software_version(self):
        """Return the software version of the device."""
        return self._software_version

    @ensure_connected
    def fetch_data(self):
        """Fetch actual device data."""
        raise NotImplementedError
