"""Module defining CGP1W device."""
import time
import typing

from mqtt_hass_base.device import MqttDevice

from mqtt_ble.common import BTDevice

ENTITIES = [
    {
        "name": "temperature",
        "device_class": "temperature",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:thermometer",
        "unit": "C",
    },
    {
        "name": "humidity",
        "device_class": "humidity",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:water-percent",
        "unit": "%",
    },
    {
        "name": "pressure",
        "device_class": "pressure",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:gauge",
        "unit": "kPa",
    },
    {
        "name": "battery",
        "device_class": "battery",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:battery-bluetooth",
        "unit": "%",
    },
]

_UUID_SERVICE_GENERIC_ACCESS = "00001800-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_NAME = "00002A00-0000-1000-8000-00805F9B34FB"

_UUID_SERVICE_DEVICE_INFO = "0000180A-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_MANUFACTURER = "00002A29-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_MODEL = "00002A24-0000-1000-8000-00805F9B34FB"
_UUID_CHAR_FIRMWARE = "00002A26-0000-1000-8000-00805F9B34FB"

_UUID_SERVICE_DATA = "22210000-554A-4546-5542-46534450464D"
_UUID_CHAR_DATA = "00000100-0000-1000-8000-00805F9B34FB"
_UUID_DESC_CCCD = "00002902-0000-1000-8000-00805F9B34FB"


class ThermometerData:  # pylint: disable=too-few-public-methods
    """Device data class."""

    humidity: typing.Optional[float] = None  # pylint: disable=unsubscriptable-object
    temperature: typing.Optional[float] = None  # pylint: disable=unsubscriptable-object
    pressure: typing.Optional[float] = None  # pylint: disable=unsubscriptable-object
    battery: typing.Optional[float] = None  # pylint: disable=unsubscriptable-object


class Cgp1wBT(BTDevice):
    """Device class."""

    def _save_handles(self, characteristics):
        """Read handles."""


class Cgp1w(MqttDevice):
    """CGP1W MQTT device class."""

    def __init__(self, config, logger):
        """Create a new MQTT CGP1W sensor object."""
        MqttDevice.__init__(self, config["name"], logger)
        self.config = config
        self._update_interval = config["update_interval"]
        self._last_data_time = 0
        self.client = Cgp1wBT(config["mac"], self.logger, notification_timeout=30)
        self._ready = False
        self._failed_connection = 0

    @property
    def ready(self):
        """Return True if the device is ready."""
        return self._ready

    @property
    def is_passive(self):
        """Return True if it's a passive BLE device."""
        return True

    def push_data(self, advertisement_data):
        """Push data to MQTT."""
        self._last_data_time = time.time()
        raw = advertisement_data.service_data["0000fdcd-0000-1000-8000-00805f9b34fb"]
        self.logger.debug("Raw advertisement_data %s", raw)
        data = ThermometerData()
        # HUM
        data.humidity = int.from_bytes(raw[12:][:2], byteorder="little") / 10
        # PRES
        data.pressure = int.from_bytes(raw[14:18][2:], byteorder="little") / 100
        # BAT
        data.battery = raw[-1]
        # TEMP
        temp = int.from_bytes(raw[8:12][2:], byteorder="little")
        if temp > 65000:
            temp = temp - 65535
        data.temperature = round(temp / 10.0, 2)
        self.logger.debug("H: %s", data.humidity)
        self.logger.debug("T: %s", data.temperature)
        self.logger.debug("P: %s", data.pressure)
        self.logger.debug("B: %s", data.battery)

        for entity_base_settings in ENTITIES:
            name = entity_base_settings["name"]
            if not hasattr(data, name):
                continue
            state = getattr(data, name)
            getattr(self, name).send_state(state)
            getattr(self, name).send_available()

    async def init_connection(self):
        """Init the BT connection to the device.

        Passive device does nothing.
        """
        await self._get_device_info()
        self._add_entities()
        return

    async def _get_device_info(self):
        """Get device informations."""
        self.logger.info("Getting device info for %s", self.name)
        await self.client.device_info()

        self.mac = self.config["mac"]
        self.manufacturer = self.client.manufacturer
        self.sw_version = self.client.firmware_version
        self.model = self.client.model
        self._ready = True

    def _add_entities(self):
        """Create sensors."""
        for entity_base_settings in ENTITIES:
            cleaned_name = " ".join(
                (
                    "mqtt-ble",
                    self.config["name"].lower().replace(" ", "_"),
                    entity_base_settings["name"],
                )
            )
            entity_settings = entity_base_settings.copy()
            del entity_settings["name"]
            setattr(
                self,
                entity_base_settings["name"],
                self.add_entity(
                    "sensor",
                    cleaned_name,
                    entity_settings,
                ),
            )

    def check_availability(self):
        """Define availability."""
        last_data_received = time.time() - self._last_data_time
        self.logger.debug("Device seen %d seconds ago", last_data_received)
        if last_data_received < self._update_interval:
            # Available
            for entity_base_settings in ENTITIES:
                name = entity_base_settings["name"]
                getattr(self, name).send_available()
        else:
            # Not Available
            self.logger.warning(
                "Settings device not available, last seen %d seconds ago",
                last_data_received,
            )
            for entity_base_settings in ENTITIES:
                name = entity_base_settings["name"]
                getattr(self, name).send_not_available()

    async def disconnect(self):
        """Disconnect from device."""
        self.logger.info("BLE disconnecting")
        await self.client.disconnect()
